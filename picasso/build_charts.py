'''Chart builder'''
import json
import logging
import os
import tornado.options
from pymongo.errors import AutoReconnect
from pymongo.collection import Collection 
from tornado.options import options

import picasso

import picasso.options
from picasso.connections import connect, connection
from picasso.exceptions import *
from picasso.model import Country
from picasso.stats import get_stats, save_stats
from picasso.charts import get_chart, charts
from picasso.charts.donut import get_historic_data
from picasso.renderchart import render_chart

def read_config():
    '''Parse command line and config file options'''
    tornado.options.parse_command_line()
    if not options.config:
        raise ConfigNotFoundException()
    tornado.options.parse_config_file(options.config)

def init():
    '''Initialize all the db structures'''
    psql_conf = {
        'database': options.pgsql_database,
        'user': options.pgsql_user,
        'host': options.pgsql_host,
        'password': options.pgsql_password,
        'port': options.pgsql_port
    }

    sphinx_conf = {
        'host': options.sphinx_host,
        'port': options.sphinx_port
    }

    mongo_conf = {
        'host': options.mongo_host,
        'port': options.mongo_port
    }

    connect(psql_conf, sphinx_conf, mongo_conf)

def print_chart_fail(chartname, reason=""):
    print_chart_msg(chartname, success=False, msg=reason)

def print_chart_msg(chartname, success=True, msg=None):
    ''' Print whether the chart was successfully built and saved'''
    if success:
        logging.info("{}: OK!".format(chartname))
    else:
        logging.warn("{}: FAILED (Reason:{})".format(chartname, msg))

def _page_ids():
    """Get a list of merchants for whom build charts"""
    statsdb = connection().mdbase.merchants_stats
    if not options.cid:
        raise MissingCountryError('cid is required')
    pids = sorted(
        [
            item for item 
            in statsdb.find(
                {'country_id':int(options.cid)}
            ).distinct('page_id')
        ]
    )
    ccode = Country.get_ccode(options.cid)
    return pids, ccode, options.cid

def save_graph(graphsdb, oid, chartname, data, svg, cid):
    """Save graph data to mongo, if any."""
    max_retries = 3
    if svg:
        document = {
            'id': int(oid),
            chartname: svg,
            '{}_data'.format(chartname): json.dumps(data),
            'country_id': cid
        }
        for i in xrange(max_retries):
            try:
                graphsdb.update(
                    {'id': document['id']},
                    {'$set': document},
                    upsert=True
                )
                print_chart_msg(chartname)
                logging.info("Chart .svg saved to database")
                break
            except Exception, exc:
                print_chart_fail(chartname, reason="Exception ({0}) raised".format(exc))
                if i < max_retries -1:
                    logging.info("Retrying")
                continue
        else:
            print_chart_fail(chartname, reason="Max retries reached".format(exc))
    else:
        print_chart_msg(chartname, success=False, msg="Rendered SVG is empty")

def build_them_charts(mdbase, charts_list):
    statsdb = mdbase.merchants_stats
    graphsdb = Collection(mdbase, 'graphs')
    pids, ccode, cid = _page_ids()
    logging.info("Found {0} pages".format(len(pids)))
    if not charts_list:
        logging.warn("No charts specified, will only collect stats.")
    for page_id in pids:
        logging.info("Building charts for page #{0}".format(page_id))
        os.system("mkdir -p {tmp}/aux/{pid} {tmp}/results/{pid}".format(
            tmp=options.temp_dir, pid=page_id)
        )
        stats = get_stats(statsdb, page_id)
        if stats:
            if options.json:
                logging.info(stats)
            if not options.skip_save:
                save_stats(stats, graphsdb, page_id)
        else:
            logging.warn(
                "#{pid} has no data available for stats".format(pid=page_id)
            )
        if charts_list:
            data = get_historic_data(page_id, ccode)
            if data and not options.skip_save:
                document = {
                    'id': int(page_id),
                    'donut_historic': json.dumps(data),
                    'country_id': options.cid,
                }
                for i in range(3):
                    try:
                        graphsdb.update(
                            {'id':int(page_id)},
                            {'$set': document},
                            upsert=True
                        )
                        break
                    except AutoReconnect:
                        continue
        for chartname in charts_list:
            logging.info("Building {0} for page {1}".format(chartname, page_id))
            chart_gen = get_chart(chartname)
            # get data
            data = chart_gen.get_data(page_id=page_id, ccode=ccode)
            if not data:
                print_chart_msg(chartname, success=False, msg="No data")
                continue
            # use data to render
            svg = render_chart(page_id, ccode, chartname, data) 
            # save rendered chart
            if not options.skip_save:
                save_graph(graphsdb, page_id, chartname, data, svg, ccode)


def main():
    """Build graphs specified by commandline parameters"""
    try:
        read_config()

        init()

        conn = connection()
        mdbase = conn.mdbase

        if not os.path.exists(options.work_dir):
            logging.error(
                "".join([options.work_dir, "does not exists. Aborting"])
            )
            raise IOError

        # Everything starts here
        # statsdb = mdbase.merchants_stats
        # graphsdb = Collection(mdbase, 'graphs')
        # Select which charts to build
        if options.all:
            charts_list = charts.all_charts()
        else:
            charts_list = [item.strip().lower() 
                for item in options.charts.split(",") if item
            ]
        # Select merchant_ids to build charts for
        build_them_charts(mdbase, charts_list)

        logging.info("All is done")
        return 0

    except ConfigNotFoundException:
        tornado.options.print_help()
        raise

    except OptionNotFoundException:
        tornado.options.print_help()
        raise

if __name__ == '__main__':
    main()
