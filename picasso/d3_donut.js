var d3_donut = (function($, d3){
	var module = {};
	///Constants
	module.max_width = 350;
	module.max_height = 350;
	module.margin = {top: 30, right: 30, bottom: 30, left: 30};
	module.in_radius = 50; // Inner radius
	module.gen_radius = 50; // General radius size
	module.out_radius = module.in_radius + module.gen_radius; // Outer radius
	module.pad_radius = Math.PI*0.0125;
	module.max_radius = module.out_radius + 80; // Max radius of donut
	module.cutoff_percentage = 10;

	var container = '#pie-chart',
		canvas = d3.select(container + " svg"),
		chart = canvas.select("g"),
		donut = chart.select("g"),
		pie_data = null, //Data ready for arc
		main_arc = null, //Main arc generator
		percent_arc = null, //Percent arc generator
		label_arc = null, //Labels arc generator
		original_data = null, //Original data if it gets replaced(the latest one)
		current_data = null, //Data actually being rendered
		prev_data = null, //Previous data
		prev_pie = null, //Previous pie data, ready for arc
		alert_count = null; //Alert count for signaling when all things were rendered

	function makeStucture(){
		canvas = d3.select(container + " svg");
		if(canvas.empty()){
			canvas = d3.select(container).append("svg")
				.attr("width", "100%")
				.attr("height", "100%")
				.attr("viewBox", "0 0 " + module.max_width + " " + module.max_height)
				.attr("preserveAspectRatio", "xMinYMin")
				.attr("chart-version", "1.1");
			chart = canvas.append("g")
				.attr("transform", "translate(" + module.margin.left + "," + module.margin.top + ")");
			donut = chart.append("g")
				//.attr("transform", "translate(" + (module.max_width/2 - module.margin.left) + ", " + (module.max_height/2 - module.margin.top) + ")")
				.attr("transform", "translate(" + (module.max_radius - module.margin.left) + ", " + (module.max_radius - module.margin.top) + ")")
				.attr("class", "donut-pie");
		}
		else{
			chart = canvas.select("g");
			donut = chart.select("g");
		}
	}

	// Private methods
	function parseLabel(keyname){
		var words = keyname.split(" ");
		var res = [];
		var word_accum = "";
		for(var i = 0; i < words.length; ++i){
			if(word_accum.length + words[i].length < 12){
				word_accum += words[i] + " ";
			}
			else{
				res.push(word_accum);
				word_accum = words[i] + " ";
			}
		}
		if(word_accum.length > 0){
			res.push(word_accum)
		}
		return res;
	}

	function sortFunction(d1, d2){
		// Ensure that last value is always "Others" 
		if(d1['id'] == -1 || d2['id'] == -1){
			return d1['id'] == -1? 1 : -1;
		}
		return d3.descending(d1.value,d2.value);
	}

	function midAngle(d){
		return d.startAngle + ((d.endAngle - d.startAngle)/2); 
	}

	function alertDone(){
		if(++alert_count == pie_data.length){
			$(document).trigger("done");
		}
	}

	function translateString(x_offset, y_offset){
		return "translate(" + x_offset + ", " + y_offset + ")";
	}

	function dataJoin(selector, data){
		return selector.data(data);
	}

	function isConflicted(d, i){
		return (d.data.percent < 4) ||
			(
				(d.data.percent < 6) &&
				i > 0 && (pie_data[i-1].data.percent - d.data.percent) < 3
			);
	}

	function isLowerHalf(d, i){
		return midAngle(d) < (1.5*Math.PI) && midAngle(d) > (0.5*Math.PI);
	}

	function isLabelOutOfCanvas(d, i){
		var label = d.data.key,
			label_width = 6.8*parseLabel(label)[0];
		if(midAngle(d) < Math.PI){
			return label_arc.centroid(d)[0] + label_width > module.max_width/2;
		}
		else{
			return label_arc.centroid(d)[0] - label_width < module.max_width/2;
		}
	}

	function moveToggle(elem){
		var old_elem = d3.select('path.selected');
		old_elem
			.classed("selected", false);
		if(elem){
			old_elem
				.transition()
				.duration(200)
					.attr("transform", translateString(0,0));
		}
		else{
			//Make it instantaneous
			old_elem.attr("transform", translateString(0,0));
		}
		if(elem){
			elem
				.classed("selected", true)
				.transition()
				.duration(200)
					.attr("transform", function(d, i){
						var c = main_arc.centroid(d),
							x = c[0],
							y = c[1],
							h = Math.sqrt(x*x + y*y),
							offset = 5;
						return translateString((x/h) * offset, (y/h) * offset);
					});
		}
	}

	function drawBackButton(){
		var backbutton = chart.append("g")
			.attr("class", "back-button")
			.attr("transform", translateString(module.max_width -100, 0));

		var backbutton_window = backbutton.append("rect")
			.attr("fill", "rgba(204, 204, 204, 0.8)")
			.attr("height", 30)
			.attr("width", 80);

		backbutton.append("text")
			.attr("x", 8)
			.attr("y", 20)
			.attr("fill", "#333")
			.text("Go back");

		backbutton.on("click", function(d, i){
			var e = d3.event;
			e.stopPropagation();
			module.change(prev_data);
			try{
				$(d3_histogram.container() + " svg").triggerHandler("go-back");
			}
			catch(e){
				console.log("Can't find time-chart");
			}
			//$("#time-chart svg").triggerHandler("go-back");
			backbutton.remove();
			//return false;
		});
	}

	module.debug = function(){
		console.log(canvas);
		console.log(chart);
		console.log(donut);
		return !canvas.empty();
	}

	// Public methods
	module.data = function(){
		return pie_data;
	}

	module.container = function(new_container){
		if(new_container === undefined){
			return container;
		}
		else{
			container = new_container;
			makeStucture();
		}
	}

	module.rebind = function(data){
		module.calculate(data);
		dataJoin(donut.selectAll("g.label-paths"), pie_data);
		dataJoin(donut.selectAll("path.main"), pie_data);
		dataJoin(donut.selectAll("g.labels"), pie_data);
		dataJoin(donut.selectAll("g.percent"), pie_data);
		module.eventHandlers();
	}

	module.change = function(data){
		//Save old status
		prev_pie = module.data();
		prev_data = current_data || original_data;
		//Update data
		current_data = data;
		moveToggle();
		module.calculate(data);
		chart.select("g.back-button").remove();
		module.draw();
		module.eventHandlers();
	};

	module.restoreOriginal = function(){
		module.change(original_data);
	}

	module.calculatePie = function(data){
		var pad_radius = module.pad_radius;
		var pie = d3.layout.pie()
			.sort(sortFunction)
			.value(function(d){return d.percent;})
			.padAngle(pad_radius);
		//Filter data with 0 percent, won't show up in chart, anyway
		data = data.filter(function(e){return e.percent > 0;}); 
		data.sort(sortFunction); //Sort data (for convenience)
		original_data = original_data || data; //Backup original data
		pie_data = pie(data); //Data ready for arc shape
	};

	module.calculate = function(data){
		module.calculatePie(data); //Get pie layout
		//If there are too many labels, some of them will probabably 
		// fall out chart, so we reduce the pie radius size 
		var mod = (data.length > 6)?20:0;
		var in_radius = module.in_radius - mod,
			gen_radius = module.gen_radius - mod,
			out_radius = module.out_radius - mod;
		//Setup arc generators
		main_arc = d3.svg.arc() // Setup arc generator
			.innerRadius(in_radius)
			.outerRadius(in_radius + gen_radius);
		percent_arc = d3.svg.arc()
			.innerRadius(in_radius + gen_radius/2)
			.outerRadius(in_radius + gen_radius/2);
		label_arc = d3.svg.arc()
			.innerRadius(out_radius + 5)
			.outerRadius(out_radius + 5);
	};

	function draw_label_lines(labels_offsets){
		var label_paths = donut.selectAll("g.label-paths").data(pie_data),
			safe_radius = 0;
		debug = label_paths;

		//Remove data that was left out
		label_paths.exit().remove();

		//Recreate structure for entering elements, adding them to UPDATE
		label_paths.enter()
			.append("g")
				.classed("label-paths", true)
				.append("line");

		//Modify UPDATE
		label_paths
			.attr("data-id", function(d, i){return d.data.id;})
			.select("line")
				.attr("x1", function(d, i){return main_arc.centroid(d)[0];})
				.attr("y1", function(d, i){return main_arc.centroid(d)[1];})
				.each(function(d, i){
					var that = d3.select(this),
						label_width = 6*parseLabel(d.data.key)[0].length,
						inner_radius = label_arc.innerRadius()(d);
					// console.log(d);
					// console.log(d.data.key);
					// console.log(label_arc.centroid(d)[0]);
					// console.log(6*parseLabel(d.data.key)[0].length);
					// console.log(label_arc.innerRadius()(d));
					// It collides with other labels
					if(i > 0 && isConflicted(d, i)){
						//Offset necessary to clear previous label position
						var prev_label_pos = 11*parseLabel(pie_data[i-1].data.key).length;
						//Increase radius for other labels
						safe_radius += prev_label_pos;
						var c = label_arc.centroid(d),
							x = c[0],
							y = c[1],
							h = Math.sqrt(x*x + y*y),
							offset = h + safe_radius;
						that
							.attr("x2", (x/h)*offset)
							.attr("y2", (y/h)*offset);
						labels_offsets[String(d.data.id)] = [(x/h)*offset, (y/h)*offset];
					}
					//Label doesn't fit the canvas
					else if(isLabelOutOfCanvas(d, i)){
						var angle = d.endAngle - d.startAngle;
						var angle_modifier = isLowerHalf(d, i)?-1:1; 
						//Move the label of out the way, slightly
						var new_slice = {
							'startAngle': d.startAngle + (angle_modifier)*0.25*angle,
							'endAngle': d.endAngle + (angle_modifier)*0.25*angle
						},
							x_offset = label_arc.centroid(new_slice)[0],
							y_offset = label_arc.centroid(new_slice)[1];
						that
							.attr("x1", main_arc.centroid(new_slice)[0])
							.attr("y1", main_arc.centroid(new_slice)[1])
							.attr("x2", x_offset)
							.attr("y2", y_offset);
						labels_offsets[String(d.data.id)] = [x_offset, y_offset];
					}
					else{
						that
							.attr("x2", label_arc.centroid(d)[0])
							.attr("y2", label_arc.centroid(d)[1]);
						labels_offsets[String(d.data.id)] = label_arc.centroid(d);
					}
				});
	}

	function draw_slices(){
		var paths = donut.selectAll("path.main").data(pie_data);

		paths.exit().remove();

		// paths.enter().append("path")
		// 	.attr("class","main");
			
		paths
			// .attr("fill", function(d,i){return colors(i)})
			.attr("data-value", function(d, i){return d.data.value})
			.attr("data-key", function(d, i){return d.data.key})
			.attr("data-id", function(d, i){return d.data.id})
			.each(function(d,i){
				var that = d3.select(this);
				if(prev_pie[i]){
					var src_start = prev_pie[i].startAngle;
					var src_end = prev_pie[i].endAngle;
				}
				else{
					var src_start = 0;
					var src_end = 0;
				}
				var save_start = d.startAngle;
				var save_end = d.endAngle;
				//Now transition to actual position
				that.transition().duration(700)
					.attrTween("d", function(d, i, a){
						// Must return an interpolator
						return function(t){
							d.startAngle = (1-t)*src_start + save_start*t //Interpolate start
							d.endAngle = (1-t)*src_end +save_end*t //Interpolate end
							return main_arc(d,i) //Return generated values with interpolated start and end
						}
					});
				// Wait for transition to end
				d3.timer(function(){
					alertDone();
					return true;
				}, 1000);
			});

		paths.enter().append("path")
			.attr("class","main")
			.attr("data-value", function(d, i){return d.data.value})
			.attr("data-key", function(d, i){return d.data.key})
			.attr("data-id", function(d, i){return d.data.id})
			.each(function(d,i){
				var that = d3.select(this);
				var src_start = pie_data[pie_data.length-1].startAngle;
				var src_end = pie_data[pie_data.length-1].endAngle;
				var save_start = d.startAngle;
				var save_end = d.endAngle;
				//Now transition to actual position
				that.transition().duration(700)
					.attrTween("d", function(d, i, a){
						// Must return an interpolator
						return function(t){
							d.startAngle = (1-t)*src_start + save_start*t //Interpolate start
							d.endAngle = (1-t)*src_end +save_end*t //Interpolate end
							return main_arc(d,i) //Return generated values with interpolated start and end
						}
					});
				// Wait for transition to end
				d3.timer(function(){
					alertDone();
					return true;
				}, 1000);
			});
	}

	function draw_labels(labels_offsets){
		donut.selectAll("g.labels").remove();

		var pie_labels = donut.selectAll("g.labels").data(pie_data);

		pie_labels.enter().append("g").attr("class", "labels");

		pie_labels
			.attr("data-id", function(d, i){return d.data.id;})
			.attr("transform", function(d, i){
				return "translate(" + labels_offsets[String(d.data.id)] + ")";
			})
			.each(function(d, i){
				var that = d3.select(this),
					label_lines = parseLabel(d.data.key);
				for(var w = 0; w < label_lines.length; ++w){
					that.append("text")
						.attr("text-anchor", function(d, i){
							return midAngle(d) < Math.PI?
							"start":"end";
						})
						.attr("transform", function(d,i){
							if(isLowerHalf(d,i)){
								return translateString(0, (10 + 15*w));
							}
							else{
								return translateString(0, 
									-(15 *((label_lines.length - 1) - w))
								);
								
							}
						})
						.text(function(d, i){return label_lines[w]});
				}
			});
	}

	function draw_percents(){
		donut.selectAll("g.percent").remove();

		var percents = donut.selectAll("g.percent").data(pie_data);

		percents.enter().append("g").attr("class", "percent");

		percents
			.attr("data-id", function(d, i){return d.data.id;})
			.attr("transform", function(d, i){
				return "translate(" + percent_arc.centroid(d) + ")";
			})
			.append("text")
				.text(function(d, i){
					return (d.data.percent < 10)?
						null : d.data.percent + "%";
				});
	}

	module.draw = function(){
		var labels_offsets = {};

		alert_count = 0;
		//console.log("Drawing label_lines");
		draw_label_lines(labels_offsets);
		//console.log("Drawing slices");
		draw_slices();
		//console.log("Drawing labels");
		draw_labels(labels_offsets);
		//console.log("Drawing percents");
		draw_percents();
	};

	module.eventHandlers = function(){
		var chart = $(container + " svg");
	
		// Remove all events previously attached
		chart.off('.slice .sliceselected');
		//$("#pie-chart").off('mousemove');

		chart.on('click.slice', 'path.main', function(e){
			var that = d3.select(this);
			var datum = that.datum();
			//Interactivity with other charts
			try{
				$(d3_histogram.container() + " svg")
					.triggerHandler("get-historic", 
						{id: datum.data.id, elem: this}
					);
			}
			catch(e){
				console.log("Can't find time-chart");
			}
			if(pie_data.length != 1){
				if(datum.data.id == -1){
					// Expand subcategory
					var new_data = datum.data.subcats;
					if(new_data.length)
					{
						$(container + ' .tooltip')
							.fadeOut(function(){
								$(this).removeClass("visible");
							});
						module.change(new_data);
						drawBackButton();
					}
				}
				else{
					// Select slice
					moveToggle(that);
				}
				return false;
			}
		});
		// Unselect slice
		chart.on('click.sliceselected','path.main.selected', function(e){
			var that = d3.select(this);
			that
				.classed("selected", false)
				.transition()
				.duration(200)
					.attr("transform", translateString(0,0));
			return false;
		});

		// Tooltip events
		var tooltip = $(container + ' .tooltip');

		chart.on("mouseover.slice", "path.main, .percent", function(e){
			var that = $(this),
				d = d3.select(this).datum().data;
			tooltip
				.addClass("visible")
				.stop(true)
				.fadeIn()
			tooltip.children(".key").text(d.key);
			tooltip.children(".value").text(d.value);
			tooltip.children(".percent").text("(" + d.percent + "%)");
		});

		chart.on("mouseout.slice", "path.main", function(e){
			var that = $(this);
			tooltip
				.delay(400)
				.fadeOut(function(){
					$(this).removeClass("visible");
				});
		});
		chart.on("mousemove.slice", function(e){
			var that = $(this),
				offsetX = e.pageX - that.offset().left,
				offsetY = e.pageY - that.offset().top;
			if(tooltip.hasClass("visible")){
				tooltip
					.css("top", offsetY -20)
					.css("left", offsetX + 25);
			}
		});	
	}
	return module;
}($, d3));
