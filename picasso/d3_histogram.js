var d3_histogram = (function($, d3){
	var module = {};
	///Constants
	module.max_width = 600;
	module.max_height = 220;
	module.margin = {top: 10, right: 10, bottom: 40, left: 40};
	module.width = (module.max_width - module.margin.left - module.margin.right);
	module.height = (module.max_height - module.margin.top - module.margin.bottom);

	var container = '#time-chart',
		canvas = d3.select(container + " svg"),
		chart = canvas.select("g"),
		current_data = null, //Data 
		prev_data = null,
		original_data = null, //Original data if it gets replaced
		alert_count = null, //Alert count for signaling when all things were rendered
		x_scale = null,
		y_scale = null,
		prev_x_scale = null,
		prev_y_scale = null,
		line = null,
		x_axis = null,
		y_axis = null;

	function makeStucture(){
		canvas = d3.select(container + " svg");
		if(canvas.empty()){
			canvas = d3.select(container).append("svg")
				.attr("width", "100%")
				.attr("height", "100%")
				.attr("viewBox", "0 0 " + module.max_width + " " + module.max_height)
				.attr("preserveAspectRatio", "xMinYMin")
				.attr("chart-version", "1.1");
			chart = canvas.append("g")
				.attr("transform", "translate(" + module.margin.left + "," + module.margin.top + ")");
		}
		else{
			chart = canvas.select("g");
		}
	}

	function alertDone(){
		if(++alert_count == current_data.length){
			$(document).trigger("done");
		}
	}

	function translateString(x_offset, y_offset){
		return "translate(" + x_offset + ", " + y_offset + ")";
	}

	function dataJoin(selector, data){
		return selector.data(data);
	}

	function findMaxValue(data, yAccessor){
		if(yAccessor === undefined){
			yAccessor = module.yAccessor;
		}
		return d3.max(data, yAccessor);
	}

	function yAccessor_default(d, i){
		return d.value;
	}

	function allKeys(data){
		var res = [],
			i;
		for(i = 0; i < data.length; ++i){
			res.push(data[i].key);
		}
		return res;
	}

	function findById(id, data){
		var found = undefined;
		for(var j = 0; j < data.length; ++j){
			var item = data[j];
			if(id != -1 && item.id == -1){
				found = findById(id, item.subcats);
				if(found) break;
			}
			else if(item.id == id){
				found = item;
				break;
			}
		}
		return found;
	}

	// Public methods
	module.x = function(){
		return x_scale;
	};
	module.y = function(){
		return y_scale;
	};

	module.debug = function(){
		console.log("Canvas:", canvas);
		console.log("Chart:", chart);
		console.log("x", current_data.map(function(d){return x_scale(d.key);}));
		return !canvas.empty();
	};

	module.data = function(){
		return current_data;
	};

	module.container = function(new_container){
		if(new_container === undefined){
			return container;
		}
		else{
			container = new_container;
			makeStucture();
		}
	};

	module.rebind = function(data){
		module.calculate(data);
		dataJoin(chart.selectAll("g"), current_data);
		module.eventHandlers();
	};

	module.change = function(data){
			module.calculate(data);
			module.draw();
			module.drawLine(function(d, i){return d.value['price_avg']});
			module.eventHandlers();
	};

	module.restoreOriginal = function(){
		module.change(original_data);
	}

	module.yAccessor = yAccessor_default;

	module.calculate = function(data){
		var new_x, new_y;
		original_data = original_data || data;
		prev_data = current_data || original_data;
		current_data = data;
		new_x = d3.scale.ordinal()
			.domain(allKeys(current_data))
			.rangeRoundBands([0, module.width], 0.2, 0.3);
		prev_x_scale = x_scale || new_x;
		x_scale = new_x; 
		new_y = d3.scale.linear()
			.domain([0, findMaxValue(
				current_data, function(d, i){
					return d.value.price_avg + d.value.discount_avg
					}
				)
			])
			.range([module.height, 0]);
		prev_y_scale = y_scale || new_y;
		y_scale = new_y;
		x_axis = d3.svg.axis().scale(x_scale).orient('bottom')
			.tickFormat(function(d,i){
				var this_month = current_data[i].data.monthname;
				if(i==0 || current_data[i-1].data.monthname != this_month){
					return this_month;
				}
				else{
					return "";
				}
			});
		y_axis = d3.svg.axis().scale(y_scale).orient("left");

	};

	module.drawLine = function(y_accessor){
		line = d3.svg.line().interpolate('basis')
			.x(function(d, i){return x_scale(d.key);})
			.y(function(d, i){return y_scale(y_accessor(d, i));});
		var prev_line = d3.svg.line().interpolate('basis')
			.x(function(d, i){return prev_x_scale(d.key);})
			.y(function(d, i){return prev_y_scale(y_accessor(d, i));});

		line_selection = chart.select('path.line');
		
		if(line_selection.empty()){
			chart.append("svg:path")
				.attr("class", "line")
				.attr("transform", "translate(" + x_scale.rangeBand()/2 + ",0)");
		}

		line_selection.transition()
			.attrTween("d", function(d, i, a){
				return d3.interpolate(prev_line(prev_data), line(current_data));
			});
	}

	module.draw = function(){
		alert_count = 0;

		//chart.selectAll("g").remove();
		// Create a bar for each data point
		var bar = chart.selectAll("g").data(current_data);

		bar.enter()
			.append("g")
				.attr("transform", function(d, i) { return "translate(" + x_scale(d.key) + ",0)"; })
				.append("rect");

		bar.select("rect")
			.attr("width", x_scale.rangeBand())
			.attr("data-day", function(d, i){return d.data.day;})
			.attr("data-key", function(d, i){return d.key;})
			.transition().duration(300)
				.attr("height", function(d) { return module.height - y_scale(module.yAccessor(d)); })
				.attr("y", function(d,i) {return  y_scale(module.yAccessor(d)); });

		//Remove both axes
		chart.selectAll("g.axis").remove();

		// X Axis
		chart.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + module.height + ")")
			.call(x_axis)
				.append("text")
					.attr("class", "axis label")
					.attr("dx", module.width/2 )
					.style('text-anchor', "middle")
					.attr('dy',"40px");
					//.text("Time (grouped by weeks)");

		// Y Axis
		chart.append("g")
			.attr("class", "y axis")
				.call(y_axis)
				.append("text")
					.attr("class", "axis label")
					.attr("transform", "rotate(-90)")
					.attr("dy", "-50")
					.attr("dx", "-" + module.height/2 + "px")
					.style("text-anchor", "middle");
					//.text("Average value and average price");

		// Highlight labeled ticks
		var tickmarks = d3.selectAll(".x.axis g.tick");
		tickmarks
			.each(function(d, i){
				var that = d3.select(this);
				if(that.select("text").text().length){
					that.select("line").attr("stroke-width", 3);
					if(i > 0 && d3.select(tickmarks[0][i-1]).select("text").text().length){
						that.select("line").attr("y2", "15");
						that.select("text").attr("y", "18");
					}
				}
			});

	};

	module.eventHandlers = function(){
		var chart = $(container + " svg");

		chart.off("get-historic go-back");

		chart.on("get-historic", function(e, params){
			//Triggered by donut chart
			if(donut_historic === undefined) return false;
			var that = this,
				new_data = [],
				oid = params['id'];
			if(oid != -1){
				if(d3_donut.data().length > 1){
					for(var i = 0; i < timechart_data.length; ++i){
						var original_key = current_data[i].key,
							value = donut_historic[original_key];
						var	elem = 
						{
							'key': original_key,
							'value':
							{
								'count': 0,
								'price_avg': 0,
								'discount_avg': 0
							},
							'data': current_data[i].data
						};
						if(value != undefined){
							var item = findById(oid, value.data);
							if(item){
								elem.value['count'] = item.value;
								elem.value['price_avg'] = item.price_avg;
								elem.value['discount_avg'] = item.discount_avg;
							}
						}
						new_data.push(elem);
					}
				}
			}
			else{
				//It's other, must add up all subcategories
				for(var i = 0; i < timechart_data.length; ++i){
					var original_key = current_data[i].key,
						value = donut_historic[original_key];
					var	elem = 
					{
						'key': original_key,
						'value':
						{
							'count': 0,
							'price_avg': 0,
							'discount_avg': 0
						},
						'data': current_data[i].data
					};
					if(value != undefined){
						var main = findById(-1, value.data);
						for(var j = 0; j < main.subcats.length; ++j){
							item = main.subcats[j];
							elem.value['count'] += item.value;
							elem.value['price_avg'] += item.price_avg * item.value;
							elem.value['discount_avg'] += item.discount_avg * item.value;
						}
						elem.value['price_avg'] /= elem.value['count'];
						elem.value['discount_avg'] /= elem.value['count'];
					}
					new_data.push(elem);
				}
			}
			if(new_data.length) module.change(new_data);
			return false;
		});
		
		chart.on("go-back", function(e){
			module.restoreOriginal();
			return false;
		});
	};
	return module;
}($, d3));
