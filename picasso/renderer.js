var system = require('system');
var fs = require('fs');
var page = require('webpage').create();
var args = system.args;
phantom.injectJs('waitfor.js');

var exitCode = 0;

var TMPDIR = "/tmp/picasso";

var Writer = {
	write: function(content, callback){
		if(!content){
			system.stdout.writeLine("Dude, there's nothing!");
			return;
		}
		system.stdout.writeLine(content);
		if(callback){
			callback();
		}
	},
	error: function(error_msg, callback){
		system.stderr.writeLine(error_msg);
		if(callback){
			callback();
		}
	}
}

// console.log(args)
if(args.length === 1){
	Writer.write("<USAGE>: phantomjs renderer.js <file or url to render> [optional_params]");
	exitCode = 1;
	phantom.exit(exitCode);
}

var url = args[1];

page.onError = function(msg, trace) {
	var msgStack = ['<ERROR>: ' + msg];
	if (trace && trace.length) {
		msgStack.push('<TRACE>:');
		trace.forEach(function(t) {
			msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function +'")' : ''));
		});
	}
	Writer.error(msgStack.join('\n'));
	exitCode = 2;
	phantom.exit(exitCode);
};

var contents;

page.open(url, function(status) {
	if(status === "success") {
		// Writer.write("Status: " + status);
		// Writer.write("Getting ready to evaluate");
		waitFor(
			function(){
				return page.evaluate(function(){
					return $(".status").attr("data-value") === "yes";
				});
			},
			function(){
				//console.log("Graph done!");
				contents = page.evaluate(function(){
					//Do something in the context of the page
					var res= {
						chartname: chartname,
						oid: oid,
						data: JSON.stringify(data)
					}
					res[chartname] = $('.' + chartname).html();
					return res;
				});
				var chartname = contents['chartname'],
					oid = contents['oid'];
				
				fs.write(TMPDIR + "/results/" + oid + "/"+ chartname + "-" + oid + ".svg", contents[chartname], 'w');
				phantom.exit(exitCode);
			},
			5000
		);
		// Writer.write("Evaluating done");
	}
	else{
		Writer.error("Can't render page '" + url + "'");
		exitCode = 1;
		phantom.exit(exitCode);
	}
	//phantom.exit(exitCode);
});
