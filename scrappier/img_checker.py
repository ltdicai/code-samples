#!/usr/bin/env python
import cPickle
import datetime
import json
import logging
import redis
import requests
import signal
import sys
import time
import tornado

from PIL import Image
from StringIO import StringIO
from tornado.options import define, options

import scrappier.model as model
from scrappier.model.images import ImageInfo, ImageUrl

define("test", default=False, help="simulate", type=bool)
define("config", default=None, help="config file", type=str)
define("debug", default=False, help="run for debug", type=bool)

logger = logging.getLogger("img_checker")
logging.getLogger("urllib3.connectionpool").setLevel(logging.CRITICAL)

class MaxRetriesException(Exception):
    pass

class GetImageError(Exception):
    pass

class CacheInvalidationError(Exception):
    pass

def is_cached(hash_str):
    return r.sismember("img_checker:hashes", hash_str)

def save_image_info(obj):
    url = obj['url']
    oid = cPickle.loads(str(obj['oid']))
    new_image = found = False
    checked = False
    res = False
    try:
        response = requests.get(url)
        status = response.status_code
        if status == 200:
            hash_str = str(hash(response.content))
            # Check if we already saved the image before
            if not is_cached(hash_str):
                existing_img = ImageInfo.objects(img_hash=hash_str).first()
                if not existing_img:
                    new_image = True
                    img_file = Image.open(StringIO(response.content))
                    params = {
                        'is_checked': True,
                        'checked_at': datetime.datetime.utcnow(),
                        'size_kb': float(response.headers.get('content-length'))/1024,
                        'img_hash': str(hash(response.content)),
                        'width': img_file.size[0],
                        'height': img_file.size[1]
                    }
                    img = ImageInfo(**params)
                    img.save()
                    pipe = r.pipeline()
                    pipe.sadd("img_checker:hashes", hash_str)
                    pipe.expire("img_checker:hashes", 3600)
                    pipe.set("img_checker:%s" % hash_str, cPickle.dumps(img))
                    pipe.expire("img_checker:%s" % hash_str, 3600)
                    pipe.execute()
                else:
                    logger.info("Already found in db, saving to cache")
                    found = True
                    img = existing_img
                    pipe = r.pipeline()
                    pipe.sadd("img_checker:hashes", hash_str)
                    pipe.expire("img_checker:hashes", 3600)
                    pipe.set("img_checker:%s" % hash_str, cPickle.dumps(img))
                    pipe.expire("img_checker:%s" % hash_str, 3600)
                    pipe.execute()
            else:
                logger.info("Already found in cache, not saving")
                # refresh cache
                r.expire("img_checker:hashes", 3600)
                r.expire("img_checker:%s" % hash_str, 3600)
                img = r.get("img_checker:%s" % hash_str)
                if img:
                    found = True
                    img = cPickle.loads(img)
                else:
                    r.srem('img_checker:hashes', hash_str)
                    raise CacheInvalidationError("Image not found in cache")
        else:
            raise GetImageError(
                "Error {} when requesting image".format(status)
            )
    except (GetImageError, requests.ConnectionError):
        logger.warn("An error ocurred when requesting image")
        new_image = False
        img = None
        status = 0
    # Update all ImageUrls refering to this image
    if new_image or found:
        db_obj = ImageUrl.objects(id=oid).first() #Find ImageUrl that was passed
        if not db_obj:
            db_obj = ImageUrl.objects(url=url, img=None).first() #Find any ImageUrl
        if db_obj:
            res = True
            db_obj.update(set__img=img, set__response_code=status, set__is_checked=True)
            db_obj.stats.update(inc__image_count__checked=1)
            if options.debug:
                r.incr("count_checks:%s" % obj['sid'])
            checked = True
            if img.size_kb > 100:
                logger.info("Counting stats for %s", db_obj.stats.id)
                db_obj.stats.add_to_field(
                    'image_stats', 'over_100k', url, custom=True, unique=True
                )
                db_obj.stats.save()
        else:
            logger.warn(u"Couldn't update ImageUrl for %s", url)
    if options.debug and not checked:
        r.sadd("not_checked:%s" % obj['sid'], json.dumps(obj))
    return res

def check_image(obj):
    if options.test:
        time.sleep(4)
    url = obj['url']
    ret = False
    if ImageUrl.objects(url=url).first():
        ret = save_image_info(obj)
        if not ret:
            raise Exception("Image was not saved!")
    return ret

def mark_as_failed(obj):
    oid = cPickle.loads(str(obj['oid']))
    db_obj = ImageUrl.objects(id=oid).first()
    if db_obj:
        db_obj.update(set__failed=True)
        db_obj.stats.update(inc__image_count__failed=1)

resp = None
r = None

def run_worker():
    global resp, r
    r = redis.StrictRedis()
    resp = None
    current_item = None
    retry_count = 0
    logger.info("Start to work")
    while True:
        try:
            if resp is None:
                logger.info("Waiting for an item")
                resp = r.brpoplpush('img_queue', 'processing')
                current_item = json.loads(resp)
                logger.info("Acquired {0}".format(current_item['url']))
            else:
                retry_count += 1
                if retry_count > 3:
                    # Wrap in pipeline transaction so the commands run 
                    # atomically as to avoid inconsistent states
                    pipe = r.pipeline()
                    pipe.lrem('processing', 0, resp)
                    pipe.rpush('failed', resp)
                    pipe.execute()
                    mark_as_failed(current_item)
                    resp = None
                    retry_count = 0
                    raise MaxRetriesException("Max retries reached. Skipping")
                else:
                    logger.info("Retry({0})".format(retry_count))
            if not check_image(current_item):
                logger.info("Couldn't find any corresponding URL entry. Skipping")
            r.lrem('processing', 0, resp)
            pipe = r.pipeline()
            pipe.sadd("img_checker:finished:%s" % current_item['sid'], resp)
            pipe.expire("img_checker:finished:%s" % current_item['sid'], 3600)
            pipe.execute()
            resp = None
            retry_count = 0
        except KeyboardInterrupt:
            close()
        except Exception, e:
            logger.info("Exception raised:\n{0}".format(e))
            continue

def close():
    """Close properly so no item is lost forever"""
    logger.info("Closing properly")
    if resp:
        pipe = r.pipeline()
        pipe.lrem('processing', 0, resp)
        pipe.rpush('img_queue', resp)
        pipe.execute()
    sys.exit(0)

def signal_handler(signum, frame):
    if signum == signal.SIGTERM:
        close()

def main():
    tornado.options.parse_command_line()
    if not options.config:
        tornado.options.print_help()
        return

    tornado.options.parse_config_file(options.config)

    signal.signal(signal.SIGTERM, signal_handler)
    
    model.init()
    
    run_worker()

if __name__ == "__main__":
    main()
