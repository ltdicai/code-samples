"""
ChartJS UIModule for Tornado

A reusable UIModule for inserting charts built by ChartJS. Data is provided as a
dict of key-values, and you can specify which chart you want to be built. Also 
you can provide interactivity for filtering results.


Filter object specification:

When you click on an element in a chart, the event handler passes the label of 
that element to the filter query maker, from now on referred as `label`
`filter` is an python object that specifies a query, with a required 
attribute named 'key' and then the other attributes specify the filter on that
'key'.

{'key': <string>, <operator>: <modifier>, ...}

Supported operators:
Equality:     'eq'
Greater than: 'gt'
Lesser than:  'lt'
Not Equal:  'ne'
Greater or equal than: 'gte'
Lesser or equal than:  'lte'

Suppose that `op` is one of those operators, then: 
Supported modifiers: 
Projection: "$"              -> Query `key` `op` `label`
Increment:  "{'inc': value}" -> Query `key` `op` (`label` + value)
Decrement:  "{'dec': value}" -> Query `key` `op` (`label` - value)
Multiply:   "{'mul': value}" -> Query `key` `op` (`label` * value)
Constant:   "{'k': value}" -> Query `key` `op` value

"""
import json
import tornado.web
import logging

import scrappier.lib.helpers as h

from collections import OrderedDict

logger = logging.getLogger("charts_js_module")

class ChartJS(tornado.web.UIModule):
    def javascript_files(self):
        return ["Chart.min.js", "chartjs_manager.js"]

    def render(self, *args, **kwargs):
        container_name = kwargs.get('name')
        data = kwargs.get('data')
        chart_type = kwargs.get('chart_type')
        if not data or not chart_type:
            logger.warn("No data for rendering chart %s", container_name)
            return ""
        chart_options = kwargs.get('chart_options', {})
        filter_attr = kwargs.get('filter', {})
        event = kwargs.get('event', None)
        if event:
            event_kwargs = kwargs.get('event_kwargs', {})
            event = self.render_string(event, **event_kwargs)
        width = kwargs.get('width') or 200
        height = kwargs.get('height') or 200
        data = self._adapt_data(chart_type, data, chart_options)
        return self.render_string(
            "uimodules/chartjs.html", chart_type=chart_type, 
            data=json.dumps(data), container=container_name,
            chart_options=json.dumps(chart_options),
            filter=filter_attr, event=event,
            width=width, height=height
        )

    def _adapt_data(self, chart_type, data, options):
        if chart_type == 'bar':
            return self._adapt_data_bar(chart_type, data, options)
        elif chart_type == 'donut':
            return self._adapt_data_donut(chart_type, data, options)

    def _adapt_data_bar(self, chart_type, data, options):
        res = {}
        sub_keys = None
        if isinstance(data, dict) and data:
            # it's data in the form of key-value
            example = data.values()[0]
            if isinstance(example, list): # key: [val1,...]
                datasets_count = len(example)
            elif isinstance(example, dict): # key: {subkey1:val1,...}
                datasets_count = len(example.values())
                if isinstance(example, OrderedDict):
                    sub_keys = example.keys()
                else:
                    sub_keys = sorted(example.keys())
            else:
                datasets_count = 1 # key: value
            sorted_data = sorted(data.items(), key=lambda x: coerce_to_int(x[0]))
            res['labels'] = [elem[0] for elem in sorted_data]
            res['datasets'] = []
            for i in xrange(datasets_count):
                dataset = {
                    'label': sub_keys and sub_keys[i] or ("Dataset %d" % i),
                    'fillColor': h.get_color(i),
                    'strokeColor': "#000000",
                    'highlightFill': h.get_color(i+1),
                    'highlightStroke': "#000000",
                }
                if sub_keys:
                    sub_key = sub_keys[i]
                    dataset['data'] = [elem[1][sub_key] for elem in sorted_data]
                    dataset['subkey'] = sub_key 
                else:
                    dataset['data'] = [coerce_to_list(elem[1])[i] for elem in sorted_data]
                res['datasets'].append(dataset)
        return res

    def _adapt_data_donut(self, chart_type, data, options):
        res = []
        for i, (key, value) in enumerate(data.items()):
            dataset = {
                'color': h.get_color(i),
                'highlight': h.get_color(i + 3),
                'label': key,
                'value': int(value)
            }
            res.append(dataset)
        return res

def coerce_to_int(value):
    try:
        return int(value)
    except ValueError:
        return value

def coerce_to_list(value):
    try:
        return list(value)
    except TypeError:
        return [value]
