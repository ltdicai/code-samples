window.deferred.push(function(){
	window.ChartJsManager = (function(Chart, $){
		var module = {};

		module.rendered_charts = [];

		module.renderCharts = function(charts){
			for(var idx = 0; idx < charts.length; ++idx){
				var chart = charts[idx];
				if(chart.chart_type == 'bar'){
					_renderBar(chart);
				}
				else if(chart.chart_type == "donut"){
					_renderDonut(chart);
				}
			}
			
		}

		function _renderBar(chart_obj){
			//console.log(chart_obj);
			var oid = "#" + chart_obj.container;
			var that = $(oid);
			var ctx = document.getElementById(chart_obj.container).getContext("2d");
			var chart = new Chart(ctx).Bar(chart_obj.data, chart_obj.options);
			that.removeClass('not-rendered');
			module.rendered_charts.push({"chart": chart, "obj": chart_obj});
			//Attach event handler
			that.parent().on('click', oid, function(evt){
				evt.preventDefault();
				var bar = null,
					bar_num = null;
				var bars = chart.getBarsAtEvent(evt);
				if (bars.length > 1 && evt.offsetX){
					bar_num = 0;
					for(var idx = 0; idx < bars.length; ++idx){
						bar = bars[idx];
						//WARN: evt.offsetX may not be available in all browsers!
						if (evt.offsetX > (bar.x - bar.width/2) && evt.offsetX <= (bar.x + bar.width/2)){
							bar_num = idx;
							break;
						}
					}
				}
				else{
					bar_num = 0;
				}
				bar = bars[bar_num]
				bar_data = chart_obj.data.datasets[bar_num];
				if(bar){
					if(chart_obj.eventHandler){
						chart_obj.eventHandler(bar);
					}
					else{
						_set_filter(chart_obj.filter, bar.label, bar_data.subkey);
					}
				}
				return false;
			});
		}

		function _renderDonut(chart_obj){
			var oid = "#" + chart_obj.container;
			var that = $(oid);
			var ctx = document.getElementById(chart_obj.container).getContext("2d");
			var chart = new Chart(ctx).Doughnut(chart_obj.data, chart_obj.options);
			that.removeClass('not-rendered');
			module.rendered_charts.push({"chart": chart, "obj": chart_obj});
			that.parent().on('click', oid, function(evt){
				evt.preventDefault();
				var slice = chart.getSegmentsAtEvent(evt)[0];
				if (slice){
					if(chart_obj.eventHandler){
						chart_obj.eventHandler(slice);
					}
					else{
						_set_filter(chart_obj.filter, slice.label);
					}
				}
				return false;
			});
		}

		function _set_filter(filter, value, subkey){
			var uri = new URI(window.location);
			var key = filter.key;
			if (uri.hasQuery('filter_by')){
				filter_by = JSON.parse(uri.search(true)['filter_by']);
			}
			else{
				filter_by = {'args': {}}
			}
			if (key){
				if (filter_by.args == undefined) filter_by.args = {};
				for(var attr in filter){
					if (filter.hasOwnProperty(attr)){
						switch(attr){
							case "key":
								continue;
							case "eq":
								filter_by.args[key] = _parse_mod(filter[attr], value);
								break;
							default:
								filter_by.args[key + "__" + attr] = _parse_mod(filter[attr], value);
								break;
						}
					}
				}
				if (subkey){
					var params = subkey.split(",");
					filter_by.args[params[0]] = params[1] == "true";
				}
				uri.setQuery("filter_by", JSON.stringify(filter_by));
				uri.setQuery("content", "1");
				window.location = uri.href();
			}
		}

		function _parse_mod(mod, value){
			var res;
			if (mod == "$") return "" + value;
			if ('k' in mod){
				res = mod.k;
			}
			else if ('inc' in mod){
				res = Number(value) + Number(mod.inc);
			}
			else if ('mul' in mod){
				res = Number(value) * Number(mod.mul);
			}
			else if ('dec' in mod){
				res = Number(value) - Number(mod.dec);
			}
			else{
				res = value;
			}
			return "" + res;
		}

		return module;
	})(Chart, $);

	window.ChartJsManager.renderCharts(window.charts);
});

