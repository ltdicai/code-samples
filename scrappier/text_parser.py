#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import logging
import nltk
import nltk.corpus
import time

from collections import Counter

from heapq import nlargest
from lxml import etree
from nltk.corpus import wordnet as wn
from nltk.corpus import universal_treebanks as uni_tree
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords as nltkstop
from nltk import UnigramTagger as ut
from requests import get

import scrappier.lib.helpers as h

from scrappier.model.urlresults import UrlResult
from scrappier.model.nltk_taggers import Tagger, DoesNotExist
from scrappier.lib.seo_stopwords import SEO_stopwords

logger = logging.getLogger("TextParser")

RE_WORD = re.compile(r"\b\w\w+\b", re.U)

SUPPORTED_LANGUAGES = ("en", "es", "pt")

GENERAL_STOPWORDS = {
	"off",
}

SPECIFIC_STOPWORDS = {
	"en": SEO_stopwords
	#"es": [],
	#"pt": [u"á", u"é", u"r"],
}

STOPWORDS = {
	"en": set(nltkstop.words("english") + 
			SPECIFIC_STOPWORDS.get("en", [])
		).union(GENERAL_STOPWORDS),
	"es": set(nltkstop.words("spanish") +
			SPECIFIC_STOPWORDS.get("es", [])
		).union(GENERAL_STOPWORDS),
	"pt": set(nltkstop.words("portuguese") +
			SPECIFIC_STOPWORDS.get("pt", [])
		).union(GENERAL_STOPWORDS),
}

TAGGER_CACHE = {}

MORPHY_CACHE = {}

def request_all_text(url, granularity="text"):
	res = get(url)
	tree = etree.fromstring(
		res.text, parser=etree.HTMLParser(
			remove_blank_text=True, remove_comments=True
		)
	)
	res = h.all_text(tree)
	if granularity == "word":
		return nltk.tokenize.wordpunct_tokenize(res)
	elif granularity == "sent":
		return [sent.strip() for sent in res.split("\n")]
	else:
		return res

def get_wordnet_pos(treebank_tag):
	if treebank_tag is None:
		return None
	if treebank_tag.startswith('J'):
		return wn.ADJ
	elif treebank_tag.startswith('V'):
		return wn.VERB
	elif treebank_tag.startswith('N'):
		return wn.NOUN
	elif treebank_tag.startswith('R'):
		return wn.ADV
	else:
		return wn.NOUN

class WordCount(object):
	def __init__(self):
		self.total_word_count = 0
		self._words = {}

	def __getitem__(self, item):
		return self._words.get(item, None)

	def __setitem__(self, item, value):
		self._words[item] = value

	def __add__(self, other):
		self.total_word_count += other.total_word_count
		for word, value in other._words.items():
			if word in self._words:
				self._words[word]['count'] += value['count']
				self._words[word]['forms'] = self._join_wordforms(
					self._words[word]['forms'], value['forms']
				)
			else:
				self._words[word] = value
		# update frequencies
		for word in self._words:
			self._words[word]['freq'] = self._words[word]['count']/float(self.total_word_count)
		return self

	def _join_wordforms(self, dict1, dict2):
		counter1 = Counter(dict1)
		counter2 = Counter(dict2)
		return dict(counter1 + counter2)

	def freq_n(self, n):
		return nlargest(n, self._words.items(), key=lambda x: x[1]['freq'])

	def chart_n(self, n, keep_base=False):
		if not keep_base:
			return {
				max(value['forms'].items(), key=lambda x: x[1])[0]: value['freq']*100 
				for key, value 
				in self.freq_n(n)
			}
		else:
			return {
				max(value['forms'].items(), key=lambda x: x[1])[0]: [value['freq']*100, key]
				for key, value 
				in self.freq_n(n)
			}

	def get_baseword(self, word):
		for key, value in self._words.items():
			if key == word or word in value['forms']:
				return (key, value['forms'].keys())
		return (word, [word])

	def words(self):
		return self._words

	def add_word(self, tagged_word, lang="en"):
		self.total_word_count += 1
		word = tagged_word[0].lower()
		if word.isalpha() and word not in STOPWORDS[lang] and RE_WORD.match(word):
			base_word = cached_morphy(tagged_word, lang) or word
			try:
				self._words[base_word]['count'] += 1
				self._words[base_word]['forms'][word] += 1
			except KeyError:
				self._words[base_word] = {'count': 1, 'freq': 0, 'forms': {word: 1}}

	def calculate_freq(self):
		for word in self._words.values():
			word['freq'] = word['count']/float(self.total_word_count)

def cached_morphy(tagged_word, lang="en"):
	global MORPHY_CACHE
	try:
		tagged_word = (tagged_word[0].lower(), tagged_word[1])
		key = u"%s/%s" % tuple(tagged_word)
		base_word = MORPHY_CACHE[lang][key]
	except KeyError:
		word = tagged_word[0].lower()
		tag = tagged_word[1]
		if lang == "en":
			base_word = wn.morphy(word, get_wordnet_pos(tag))
		elif lang == "es":
			base_word = morphy_es(tagged_word)
		else:
			base_word = word
		key = u"%s/%s" % (word, tag)
		try:
			MORPHY_CACHE[lang][key] = base_word
		except KeyError:
			MORPHY_CACHE[lang] = {}
			MORPHY_CACHE[lang][key] = base_word
	logger.debug(u"%s > %s", tagged_word[0].lower(), base_word)
	return base_word

# TODO: Add morphological transformations for portuguese and spanish
def morphy_pt(tagged_word):
	word = tagged_word[0]
	tag = tagged_word[1]
	if tag == "PNOUN":
		return tagged_word[0]
	elif tag == "NOUN":
		return tagged_word[0]

ES_TRANSFORMATIONS = {
	"NOUN": {
		u"e?s": [u""],
		u"ces": [u"z"],
		u"iones": [u"ión"] 
	},
	"VERB": {
		u"(^agradezco)": [u"agradecer"],
		u"(o|as|a|amos|áis|an|ás)": [u"ar", u"er", u"ir"],
		u"([aá]ba(mos|is|n)?)": [u"ar"],
		u"(é|ás|á|emos|éis|án)": [u""],
		u"(es|e|emos|éis|en|és)": [u"er", u"ir"],
		u"(imos|ís)": [u"ir"],
	},
	"ADJ": {
		u"[ao]s?": [u""],
		u"d[ao]s?": [u"r"],
	},
}

ES_DICT = set([word.lower()
	for word in uni_tree.words("std/es/es-universal-train.conll") 
	if word.lower() not in STOPWORDS['es']
])

def morphy_es(tagged_word):
	word = get_unicode(tagged_word[0].lower())
	tag = tagged_word[1]
	if tag == "PNOUN":
		return word
	if tag is None:
		tag = "NOUN"
	try:
		for suffix, trans in ES_TRANSFORMATIONS[tag].items():
			pattern = r"%s$" % suffix
			if re.search(pattern, word):
				for tr in trans:
					candidate = re.sub(pattern, tr, word)
					if candidate in ES_DICT:
						return candidate
	except KeyError:
		pass
	return word

def parse_content(text_content):
	tree = etree.fromstring(
		text_content, parser=etree.HTMLParser(
			remove_blank_text=True, remove_comments=True
		)
	)
	try:
		language = tree.get('lang', "en")
	except StandardError:
		language = "en"
	only_text = h.all_text(tree)
	sentences = [word_tokenize(sent) for sent in only_text.split("\n")]
	logger.debug(sentences)
	wordcount = WordCount()
	lang_prefix = language.split("-")[0]
	if lang_prefix not in SUPPORTED_LANGUAGES:
		logger.info("Language '%s' not supported... yet", lang_prefix)
		return wordcount
	if lang_prefix != "en":
		tagger = get_tagger(language)
		for sentence in tagger.tag_sents(sentences):
			for word_tag in sentence:
				wordcount.add_word(word_tag, lang=lang_prefix)
	else:
		for sentence in nltk.pos_tag_sents(sentences):
			for word_tag in sentence:
				wordcount.add_word(word_tag, lang=lang_prefix)
	wordcount.calculate_freq()
	return wordcount

def get_tagger(language):
	global TAGGER_CACHE
	lang_prefix = language.split("-")[0]
	try:
		return TAGGER_CACHE[lang_prefix]
	except KeyError:
		try:
			obj = Tagger.objects(language=lang_prefix).get()
			tagger = obj.load_tagger()
		except DoesNotExist:
			obj = Tagger(language=lang_prefix)
			logger.debug("Training new tagger for '%s'", lang_prefix)
			if lang_prefix == "es":
				tagged_sentences = uni_tree.tagged_sents(fileids="std/es/es-universal-train.conll")
			elif lang_prefix == "pt":
				tagged_sentences = uni_tree.tagged_sents(fileids="std/pt-br/pt-br-universal-train.conll")
			tagger = ut(tagged_sentences)
			obj.save_tagger(tagger)
			obj.save()
		TAGGER_CACHE[lang_prefix] = tagger
		return tagger

def split_words(text):
	return RE_WORD.findall(text)

def get_unicode(string):
	if isinstance(string, unicode):
		return string
	return unicode(string, "utf-8")
