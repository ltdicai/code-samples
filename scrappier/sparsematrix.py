from collections import deque
import datetime

from scrappier.lib.helpers import reverse_dict, get_first_n

class MissingVertex(Exception):
	pass

class SparseMatrix(object):
	def __init__(self):
		self._vertices_index = dict()
		self._vertices_index_reverse = list()
		self._vertices_outlinks = dict()
		self._vertices_inlinks = dict()
		self._link_count = 0

	@classmethod
	def load(cls, keys, links):
		ext = cls()
		for key in keys:
			ext.add(key)
		for link in links:
			if not isinstance(link, tuple):
				raise TypeError
			ext.link(link[0], link[1])
		return ext

	def index(self, key):
		try:
			return self._vertices_index[key]
		except KeyError:
			return None

	def _indexes(self, keys):
		return [self._vertices_index[key] for key in keys]

	def get_key(self, index):
		try:
			return self._vertices_index_reverse[index]
		except KeyError:
			return None

	def add(self, key):
		if key not in self._vertices_index:
			new_index = len(self._vertices_index)
			self._vertices_index[key] = new_index
			self._vertices_index_reverse.append(key)

	def remove(self, key):
		if key not in self._vertices_index:
			raise MissingVertex(key)
		index = self._vertices_index[key]
		self._vertices_index_reverse[index] = None
		#print self._vertices_outlinks[index]
		#print self._vertices_inlinks[index]
		try:
			for ovtx in self._vertices_outlinks[index].copy():
				self._unlink(index, ovtx)
		except KeyError:
			pass
		try:
			for ivtx in self._vertices_inlinks[index].copy():
				self._unlink(ivtx, index)
		except KeyError:
			pass
		del self._vertices_index[key]

	def link(self, key_src, key_dst):
		vtx_src = self.index(key_src)
		vtx_dst = self.index(key_dst)
		if vtx_src is None: 
			raise MissingVertex(key_src)
		if vtx_dst is None:
			raise MissingVertex(key_dst)
		self._link(vtx_src, vtx_dst)

	def _link(self, vtx_src, vtx_dst):
		#print u"Linking (%d, %d)" % (vtx_src, vtx_dst)
		try:
			if vtx_dst in self._vertices_outlinks[vtx_src]:
				return # already linked
			self._vertices_outlinks[vtx_src].add(vtx_dst)
		except KeyError:
			self._vertices_outlinks[vtx_src] = set((vtx_dst, ))
		try:
			if vtx_src in self._vertices_inlinks[vtx_dst]:
				return
			self._vertices_inlinks[vtx_dst].add(vtx_src)
		except KeyError:
			self._vertices_inlinks[vtx_dst] = set((vtx_src, ))
		self._link_count += 1

	def unlink(self, key_src, key_dst):
		vtx_src = self.index(key_src)
		vtx_dst = self.index(key_dst)
		if vtx_src is None:
			raise MissingVertex(key_src)
		if vtx_dst is None:
			raise MissingVertex(key_dst)
		self._unlink(vtx_src, vtx_dst)

	def _unlink(self, vtx_src, vtx_dst):
		deleted = False
		#print u"unlinking (%d,%d)" % (vtx_src, vtx_dst)
		try:
			self._vertices_outlinks[vtx_src].remove(vtx_dst)
			deleted = True
		except KeyError:
			pass
		try:
			self._vertices_inlinks[vtx_dst].remove(vtx_src)
		except KeyError:
			pass
		if deleted:
			self._link_count -= 1

	def has_link(self, key_src, key_dst):
		vtx_src = self.index(key_src)
		vtx_dst = self.index(key_dst)
		if vtx_src is None:
			raise MissingVertex(key_src)
		if vtx_dst is None:
			raise MissingVertex(key_dst)
		try:
			return vtx_dst in self._vertices_outlinks[vtx_src]
		except KeyError:
			return False

	def get_outlinks(self, key):
		vtx = self.index(key)
		if vtx is None:
			raise MissingVertex(key)
		try:
			return (self.get_key(idx) for idx in self._vertices_outlinks[vtx])
		except KeyError:
			return (item for item in [])

	def get_inlinks(self, key):
		vtx = self.index(key)
		if vtx is None:
			raise MissingVertex(key)
		try:
			return (self.get_key(idx) for idx in self._vertices_inlinks[vtx])
		except KeyError:
			return (item for item in [])

	def _get_vertices_outlinks(self, vtx):
		try:
			return (idx for idx in self._vertices_outlinks[vtx])
		except KeyError:
			return (idx for idx in [])

	def _get_vertices_inlinks(self, vtx):
		try:
			return (idx for idx in self._vertices_inlinks[vtx])
		except KeyError:
			return (idx for idx in [])

	def outdegree(self, key):
		res = len(list(self.get_outlinks(key)))
		return res

	def indegree(self, key):
		return len(list(self.get_inlinks(key)))

	def calculate_depths(self):
		queue = deque()
		depths = [None] * len(self._vertices_index)
		depths[0] = 0
		queue.append(0)
		while len(queue):
			actual_vtx = queue.popleft()
			for ovtx in self._get_vertices_outlinks(actual_vtx):
				if depths[ovtx] is None:
					depths[ovtx] = depths[actual_vtx] + 1
					queue.append(ovtx)
		return reverse_dict(
			{
				self.get_key(idx): value 
				for idx, value in enumerate(depths) if value is not None
			}
		)

	def __repr__(self):
		return self.print_graph()

	def rank_by_degree(self, degree_type='outdegree', max_results=10):
		if degree_type == "outdegree":
			degrees = ((key, self.outdegree(key)) for key in self._vertices_index.keys())
		elif degree_type == "indegree":
			degrees = ((key, self.indegree(key)) for key in self._vertices_index.keys())
		else:
			raise TypeError("unknown %s parameter for degree_type")
		return get_first_n(degrees, max_results, key=lambda x:x[1], reverse=True)

	def print_graph(self, reverse=False):
		res = u"V: %d E: %d\n" % (len(self._vertices_index), self._link_count)
		for vtx, key in enumerate(self._vertices_index_reverse):
			if key is None:
				continue
			res += u"%d(%s) " % (vtx, repr(key))
			if reverse:
				res += u", ".join(["%d" % i for i in sorted(self._get_vertices_inlinks(vtx))])
			else:
				res += u", ".join(["%d" % i for i in sorted(self._get_vertices_outlinks(vtx))])
			res += u"\n"
		res += u"-"*(len(self._vertices_index)*2 + 20)
		return res

