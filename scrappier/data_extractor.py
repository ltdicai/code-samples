import datetime
import re
from lxml import etree

from scrapy import Selector
from tornado.escape import url_unescape
from urlparse import urljoin

from scrappier.model import Project, UrlResult, Stats
import scrappier.lib.helpers as h



class InvalidField(Exception):
	pass

def fix_html(htmlstring):
	return re.sub("<br>", "<br/>", htmlstring)

def get_text(htmlstring):
	"""Get all the text nodes from `htmlstring`, and concatenate them."""
	htmlstring = fix_html(htmlstring) # hack malformed html
	texts = etree.fromstring(htmlstring, parser=etree.HTMLParser()).itertext()
	return "".join(texts).strip()

class ImagePlaceholder(object):
	def __init__(self, url, alt):
		self.url = url
		self.alt = alt

	def __getitem__(self, item):
		return getattr(self, item)

	def __repr__(self):
		return "ImagePlaceholder('url':\"{0}\", 'alt':\"{1}\")".format(
			self.url.encode("utf-8"), self.alt.encode("utf-8")
		)

	def normalize_img(self, src_url):
		"""Normalize image URL by using the URL of the page that originated it.

		If self.url is a scheme-less URL then the scheme of `src_url` is used.
		"""
		# new_url = self.url
		# scheme = h.urlparse(new_url).scheme
		# if not scheme:
		# 	src_scheme = src_url.split("://")[0]
		# 	new_url = src_scheme + ":" + new_url
		# self.url = new_url
		# return self
		new_url = urljoin(src_url, self.url)
		self.url = new_url
		return self


class HTMLExtractor(object):
	@classmethod
	def _get_selector(cls, response_or_text):
		if isinstance(response_or_text, unicode):
			return Selector(text=response_or_text, type="html")
		else:
			return response_or_text.selector

	@classmethod
	def get_noindex(cls, response_or_text):
		selector = cls._get_selector(response_or_text)
		for item in selector.xpath('//head/meta[@name="robots"]/@content').extract():
			if 'noindex' in item:
				return True
		return False

	@classmethod
	def get_title(cls, response_or_text):
		selector = cls._get_selector(response_or_text)
		for item in selector.xpath('//head/title/text()').extract():
			return item

	@classmethod
	def get_last_updated(cls, response_or_text):
		selector = cls._get_selector(response_or_text)
		res = None
		for item in selector.xpath('//div[@class="last-updated"]/time/@datetime').extract():
			res = item
		return res or datetime.datetime.utcnow()

	@classmethod
	def get_description(cls, response_or_text):
		selector = cls._get_selector(response_or_text)
		for item in selector.xpath('//head/meta[@name="description"]/@content').extract():
			return item

	@classmethod
	def get_h1s(cls, response_or_text):
		selector = cls._get_selector(response_or_text)
		h1s = selector.xpath('//h1').extract()
		res = [get_text(h1) for h1 in h1s]
		return res

	@classmethod
	def get_h2s(cls, response_or_text):
		selector = cls._get_selector(response_or_text)
		h2s = selector.xpath('//h2').extract()
		res = [get_text(h2) for h2 in h2s]
		return res

	@classmethod
	def get_img_data(cls, response_or_text):
		"""We store the image url and associated data in a placeholder, until
		we are ready to store it in the database
		"""
		res = []
		selector = cls._get_selector(response_or_text)
		for img in selector.xpath('//img'):
			try:
				img_url = img.xpath('@src').extract()[0]
				if not len(img_url):
					continue
				img_alt = img.xpath('@alt').extract() and img.xpath('@alt').extract()[0] or u""
				elem = ImagePlaceholder(img_url, img_alt)
				res.append(elem)
			except IndexError:
				continue
		return res

	@classmethod
	def get_links(cls, response_or_text):
		res = set()
		selector = cls._get_selector(response_or_text)
		for link in selector.xpath('//a'):
			link_url = link.xpath('@href').extract()[0]
			res.add(url_unescape(link_url))
		return list(res)

	@classmethod
	def get_css(cls, response_or_text):
		res = set()
		selector = cls._get_selector(response_or_text)
		for link in selector.xpath('//link[@rel="stylesheet"]/@href').extract():
			res.add(url_unescape(link))
		return list(res)

	@classmethod
	def get_js(cls, response_or_text):
		res = set()
		selector = cls._get_selector(response_or_text)
		for link in selector.xpath('//script/@src').extract():
			res.add(url_unescape(link))
		return list(res)

	@classmethod
	def get_custom(cls, selector_str, response_or_text, rule='all'):
		"""Extract elements that that match with `selector_str`.

		Extract from `response_or_text` elements that match with the 
		CSS selector `selector_str`, and return a value that corresponds
		with the specified `rule`
		"""
		if rule not in ("all", "first", "count"):
			raise TypeError("invalid rule '%s'" % rule)
		selector = cls._get_selector(response_or_text)
		elems = selector.css(selector_str).extract()
		if rule == "count":
			return len(elems)
		if rule == "first":
			return elems and elems[0] or None
		return elems

class DataExtractor(object):
	#VALID_FIELDS = ('noindex', 'title', 'description', 'h1', 'h2', 'images', 'last_updated')
	methods = {
		'noindex': HTMLExtractor.get_noindex,
		'title': HTMLExtractor.get_title,
		'description': HTMLExtractor.get_description,
		'h1': HTMLExtractor.get_h1s,
		'h2': HTMLExtractor.get_h2s,
		'last_updated': HTMLExtractor.get_last_updated,
	}
	custom_fields = None

	def __init__(self, exclude_fields=None, custom_fields=None):
		if exclude_fields:
			self.fields = set(self.methods.keys()) - set(exclude_fields)
		else:
			self.fields = set(self.methods.keys())
		self.custom_fields = custom_fields

	def extract(self, response_or_text):
		return self.parse_item(response_or_text)

	def _extract_field(self, field, item):
		if field in self.methods:
			return self.methods[field](item)
		raise InvalidField(field)

	def parse_item(self, item):
		extracted_data = {}
		for field in self.fields:
			extracted_data[field] = self._extract_field(field, item)
		if self.custom_fields:
			custom_extracted = [] 
			for elem in self.custom_fields:
				custom_extracted.append({
					'name': elem['name'], 
					'value': HTMLExtractor.get_custom(
						elem['selector'], item, rule=elem['rule']
					)
				})
			extracted_data['custom_html'] = custom_extracted
		return extracted_data
