import logging
import time
from scrapy import signals
from scrapy.exceptions import NotConfigured
from scrapy.http import Request

from ..utils import benchmark, unicode_url

from scrappier.lib.stats_recv import send_status
from scrappier.lib.sparsematrix import SparseMatrix
from scrappier.model.urlresults import UrlResult
from scrappier.model.graphs import LinkGraph

logger = logging.getLogger("graph_builder")

class URLGraphMiddleware(object):
	@classmethod
	def from_crawler(cls, crawler):
		if not crawler.settings.getbool("GENERATE_URL_GRAPH"):
			raise NotConfigured
		ext = cls()
		crawler.signals.connect(ext.spider_opened, signal=signals.spider_opened)
		crawler.signals.connect(ext.spider_closed, signal=signals.spider_closed)
		return ext

	def spider_opened(self, spider):
		spider.urlgraph = SparseMatrix()

	def should_process(self, request, spider):
		"""Decide whether we add `request` to graph

		If a spider.raised_close then, we only add URLs that we've already
		seen or were requested before the raised_close signal
		"""
		if spider.raised_close:
			return request.meta.get('duplicate', False) or not request.meta.get('skip', False)
		return True # all is well, keep all requests

	def process_spider_output(self, response, result, spider):
		#logger.info(u"Constructing graph for {0}".format(unicode_url(response.url)))
		this_url = unicode_url(response.url)
		spider.urlgraph.add(this_url)
		for r in result:
			if isinstance(r, Request) and self.should_process(r, spider):
				that_url = unicode_url(r.url)
				spider.urlgraph.add(that_url)
				spider.urlgraph.link(this_url, that_url)
			yield r

	def spider_closed(self, spider, reason):
		"""Calculate depths for all collected URIs"""
		self.save_graph(spider)
		saved_items = spider.saved_items
		if spider.run_tasks:
			spider.logger.info("Calculating URL depths")
			try:
				send_status("GRAPH", {"pid": str(spider.pid)})
				for depth, urls in spider.urlgraph.calculate_depths().items():
					spider.logger.debug(u"Updating for depth {0}".format(depth))
					try:
						oids = [url.id for url in spider.saved_items.get_multiple(urls, spider.stats)]
						start = 0
						while oids[start:start + 512]:
							UrlResult.objects(id__in=oids[start:start + 512]).update(depth=depth)
							#UrlResult.objects.in_bulk(oids[start:start + 512]).update(depth=depth)
							start += 512
					except KeyError:
						continue
			except StandardError, e:
				spider.logger.debug(e)
				spider.run_tasks = False
		else:
			spider.logger.info("Depth disabled")

	def save_graph(self, spider):
		obj = LinkGraph(stats=spider.stats)
		obj.save_graph(spider.urlgraph)
		obj.save()
