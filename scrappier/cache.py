import logging
from mongoengine.errors import MultipleObjectsReturned, DoesNotExist

from scrappier.model.images import ImageUrl
from scrappier.model.urlresults import UrlResult


class GenericCacher(object):
	"""Generic python dict that acts as a cache. It's meant to be subclassed
	so you can override `load` so it knows how to get the elements for the 
	first time."""
	_cache = {}

	def __init__(self):
		self.logger = logging.getLogger(self.__class__.__name__)

	def get(self, key, *args):
		try:
			return self._cache[key]
		except KeyError:
			return self.update(key, self.load(key, *args))

	def load(self, key, *args):
		raise NotImplementedError

	def update(self, key, value):
		if value is not None:
			self._cache[key] = value
		return value

	def remove(self, key):
		del self._cache[key]

	def clear(self):
		self._cache = {}

	def get_all(self):
		return [(key, value) for key, value in self._cache.items()]

	def __len__(self):
		return len(self._cache)

class ImageUrlCacher(GenericCacher):
	"""Cache for ImageUrl objects"""
	def load(self, key, stats):
		try:
			obj = ImageUrl.objects(stats=stats, url=key).get()
		except (MultipleObjectsReturned, DoesNotExist):
			obj = None
		return obj

	def get_multiple(self, imgs_data, stats):
		"""Warning, order is lost"""
		res = []
		missing_urls = []
		for img_data in imgs_data:
			try:
				res.append(self._cache[img_data['url']])
			except KeyError:
				missing_urls.append(img_data['url'])
		self.logger.debug("Miss rate: {0}/{1}".format(len(missing_urls), len(imgs_data)))
		return res

class UrlResultCache(GenericCacher):
	def load(self, key, stats):
		try:
			obj = UrlResult.objects(stats=stats, url=key).get()
		except (MultipleObjectsReturned, DoesNotExist):
			obj = None
		return obj

	def get_multiple(self, urls, stats):
		"""Warning, order is lost"""
		res = []
		missing_urls = []
		for url in urls:
			try:
				res.append(self._cache[url])
			except KeyError:
				missing_urls.append(url)
		self.logger.debug("Miss rate: {0}/{1}".format(len(missing_urls), len(urls)))
		if missing_urls:
			self.logger.debug("\n".join([u"%s" % item for item in missing_urls]))
		return res
